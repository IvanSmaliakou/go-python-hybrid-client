# go-python-hybrid-client

Setup: 
- install python3 for your OS
- git clone https://gitlab.com/IvanSmaliakou/go-python-hybrid-client.git
- go get -u ./...
- export PORT={PORT}
- export HOST={HOST}

Run:
- go run .