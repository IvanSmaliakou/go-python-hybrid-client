package main

import (
	"errors"
	"fmt"
	"io"
	"log"

	"github.com/valyala/fasthttp"
	"gitlab.com/go-python-hybrid-client/utils"
)

func main() {
	envs := utils.InitEnvs()
	req := fasthttp.AcquireRequest()
	resp := fasthttp.AcquireResponse()
	defer fasthttp.ReleaseRequest(req)
	defer fasthttp.ReleaseResponse(resp)
	var url = fmt.Sprintf("http://%s:%s/", envs.Host, envs.Port)

	for i := 0; i < 1000000; i++ {
		req.SetBody([]byte(fmt.Sprintf(`{"text": "test %d"}`, i)))
		req.SetRequestURI(url)
		err := fasthttp.Do(req, resp)
		if errors.Is(err, io.EOF) {
			log.Fatal("server disconnected")
		}
		fmt.Printf("%s", string(resp.Body()))
	}
}
