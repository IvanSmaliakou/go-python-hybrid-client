package utils

import "os"

type Envs struct {
	Host string
	Port string
}

func InitEnvs() *Envs {
	host := os.Getenv("HOST")
	if host == "" {
		host = "localhost"
	}
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	return &Envs{
		Host: host,
		Port: port,
	}
}
